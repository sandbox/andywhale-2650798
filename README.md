#User Import Entity Reference

Plugin for the User Import module, for handling importing entity reference fields from csv.

This plugin will also handle taxonomy term reference fields.

The plugin will match the text in the cell from the csv with any entities that could be used in the given field (if a suitable match is not found an error will be displayed and that row will not be imported).

##Is it re-usable?

Yes.